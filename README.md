# mysak-api-test
Integration testing collection for Myšák education app.
Using Postman for integration testing:

# Postman

<img src="https://www.getpostman.com/img/v2/media-kit/Logo/PNG/pm-logo-horiz.png" height="80" />

## Installation
You can get postman at https://www.getpostman.com/apps

## Import Collection
When you open postman, click on `Import` (top left menu) and find:
- `postman/mysak-api-test-collection.postman_collection.json`.

## Import Environment
When you open postman, click on `Import` (left menu) and find:
- `postman/django-mock-server-data.postman_environment.json`,
- `postman/apiary-mock-server-data.postman_environment.json`.

## Continous Integration
You can use `newman` to either run the collection and environment from files or Postman API.  
Go [here](https://learning.getpostman.com/docs/postman/postman_api/continuous_integration) for more information.